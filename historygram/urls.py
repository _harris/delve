from django.conf.urls import patterns, include, url
#from django.views.generic import DetailView, ListView

#from wordtrees import views
from historygram.views import HistorygramDetailView
from historygram import views

urlpatterns = patterns('',
    url(r'^json/(?P<query_id>\d+)/$', views.historygram_json, name='historygram_json'),
    url(r'^gram/(?P<pk>\d+)/(?P<panel_id>\d+)/$', HistorygramDetailView.as_view(), name='historygram'),
)
