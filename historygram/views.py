from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.detail import DetailView
import re, json
from django.db.models import Count

from queries.models import Query
from articles.models import Article


def historygram_json(request, query_id):
    try:
        q = Query.objects.get(id=query_id)
        records = Article.objects.filter(pmid__in=q.pmids.values_list('pmid')).values('journ_pub_year').annotate( total=Count('pmid')).order_by('journ_pub_year')
        return HttpResponse(json.dumps(list(records)), content_type="application/json")
    except Query.DoesNotExist:
        return HttpResponse("Oops.. that query does not exist." + query_id + ".")


class HistorygramDetailView(DetailView):
    model = Query
    template_name = 'historygram/index.html'

    def get_context_data(self, **kwargs):
        context = super(HistorygramDetailView, self).get_context_data(**kwargs)
        context['panel_id'] = self.kwargs['panel_id']
        return context

