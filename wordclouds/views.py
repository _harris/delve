from django.http import HttpResponse, HttpResponseRedirect
#from django.core.urlresolvers import reverse
#from django.utils import timezone
from django.views.generic.detail import DetailView
from django.db.models import Sum, Count, Min, Max

from queries.models import Query
from articles.models import Article, Meta_Freq, Meta_Bigrams, Meta_Trigrams, Mesh_Descriptor, Author, Meta_Phrases

import json

class WordCloudDetailView(DetailView):
    model = Query
    template_name = 'queries/wordcloud.html'

    def get_context_data(self, **kwargs):
        context = super(WordCloudDetailView, self).get_context_data(**kwargs)
        context['highlight'] = 'myositis' # Proof of concept?
        return context


def wordcloudjsonbytype(request, query_id, cloud_type):
    try:
        q = Query.objects.get(id=query_id)
        if cloud_type == "4":
            records = Mesh_Descriptor.objects.filter(pmid__in=q.pmids.values_list('pmid')).values('word').annotate( size=Count('pmid')).order_by('-size').values('word', 'size')[:40]
        elif cloud_type == "5":
            records = Author.objects.filter(pmid__in=q.pmids.values_list('pmid')).values('lastname').annotate(word=Min('lastname'), size=Count('pmid')).order_by('-size').values('word', 'size')[:40]
        elif cloud_type == "1":
            records = Meta_Freq.objects.filter(pmid__in=q.pmids.values_list('pmid')).values('word').annotate(size=Sum('freq')).order_by('-size')[:50]
        else:
            if cloud_type == "2":
                d = Meta_Bigrams
            elif cloud_type == "3":
                d = Meta_Trigrams
            elif cloud_type == "6":
                d = Meta_Phrases
            else:
                d = Meta_Freq
            records = d.objects.filter(pmid__in=q.pmids.values_list('pmid')).values('word').annotate(size=Sum('freq')).order_by('-size')[:40]

        return HttpResponse(json.dumps(list(records)), content_type="application/json")
    except Query.DoesNotExist:
        return HttpResponse("Oops.. that file does not exist." + query_id + " " + cloud_type + ".")

