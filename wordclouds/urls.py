from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView

from wordclouds import views
#from queries.views import WordTreeDetailView, WordCloudDetailView
    
urlpatterns = patterns('',
#    url(r'^wordtreejson/(?P<query_id>\d+)/$', views.wordtreejson, name='wordtreejson'),
#    url(r'^wordtreejson/(?P<query_id>\d+)/(?P<root>.*)/$', views.wordtreejson, name='wordtreejson'),
    url(r'^json/(?P<query_id>\d+)/(?P<cloud_type>\d+)/$', views.wordcloudjsonbytype, name='wordcloudjsonbytype'),
    )

