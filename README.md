DELVE (Document ExpLoration and Visualization Engine) is a prototype for
performing literature-based searches with the aid of interactive visualizations
and a framework for quickly implementing such visualizations as modular 
Web-applications.  The goal for DELVE is to better satisfy the information
needs of researchers and help them explore and understand the state of research
in scientific literature.
