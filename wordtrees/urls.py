from django.conf.urls import patterns, include, url
from django.views.generic import DetailView, ListView

from wordtrees import views
from wordtrees.views import WordTreeDetailView

urlpatterns = patterns('',
    url(r'^json/(?P<query_id>\d+)/(?P<root>.*)/$', views.wordtreejson, name='wordtreejson'),
    url(r'^tree/(?P<pk>\d+)/(?P<panel_id>\d+)/(?P<root>.*)/$', WordTreeDetailView.as_view(), name='wordtree'),
)

