from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.detail import DetailView
import re, json
from queries.models import Query
from articles.models import Meta_Sentence, Article

import prefix

def wordtreejson(request, query_id, root):
    root = root.lower()
    try:
        q = Query.objects.get(id=query_id)
        s = Meta_Sentence.objects.filter(pmid__in=q.pmids.values_list('pmid')).filter(sentence__icontains=root)[:100]
        l = []
        for sent in s:
            prefix.insert_sentence(l, re.sub('^.*' + root, root, sent.sentence.lower()).split(None, 2), sent.pmid.pmid)
#            prefix.insert_sentence(l, re.sub('^.*' + root, root, sent.sentence).split(" ", 3))
        prefix.merge_tails(l)
        if l:
            data = json.dumps(l[0])
        else:
            data = json.dumps({'count':'0', 'name':root})
        return HttpResponse(data, content_type="application/json")
    except Query.DoesNotExist:
        return HttpResponse("Oops.. that file does not exist.")

class WordTreeDetailView(DetailView):
    model = Query
    template_name = 'wordtrees/wordtree.html'

    def get_context_data(self, **kwargs):
        context = super(WordTreeDetailView, self).get_context_data(**kwargs)
        context['root'] = self.kwargs['root'].lower()
        context['panel_id'] = self.kwargs['panel_id']
        q = Query.objects.get(id=self.kwargs['pk'])
        s = Meta_Sentence.objects.filter(pmid__in=q.pmids.values_list('pmid')).filter(sentence__icontains=context['root'])
        context['pmids_matched_showing'] = len(set(s[:100].values_list('pmid')))
        context['pmids_matched_total'] = s.distinct('pmid').count()
        return context
