
def make_dict(word, pmid):                                                                                                                                                                                                        
        t = {}
        t['name'] = word
        t['count'] = 1
        t['pmids'] = {pmid}
        t['children'] = []
        return t

def insert_word(l, word, pmid):
        if not l:
                t = make_dict(word, pmid)
                l.append(t)
                return t['children']
        else:
                for d in l:
                        if 'name' in d and d['name'] == word:
                                d['count'] += 1
                                d['pmids'].add(pmid)
                                return d['children']
                else: #eg, if not found:
                        t = make_dict(word, pmid)
                        l.append(t)
                        return t['children']

def insert_sentence(l, s, pmid):
        if s:
                x = insert_word(l, s[0], pmid)
                insert_sentence(x, s[1:], pmid)

def merge_tails(l):
    for d in l:
        d['pmids'] = list(d['pmids']) # for compat. with json serializer
        merge_tails(d['children'])
        if d['count'] == 1 and d['children']:
            d['name'] = d['name'] + " " + d['children'][0]['name']
            d['children'] = []

